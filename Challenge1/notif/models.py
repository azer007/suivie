from django.db import models

# Create your models here.
class Reservation(models.Model):
    res = models.TextField()
    vue = models.BooleanField(default=False)

class Notification(models.Model):
    conditions = models.CharField(max_length=250,null=True)
    user = models.CharField(max_length=25,null=True)
    pwd = models.CharField(max_length=255,null=True)
    msg = models.TextField()

    def __str__(self):
        return str(self.id)


