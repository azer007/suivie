from django.urls import path
from .views import *
from .models import Notification
from django.views.generic.edit import UpdateView


app_name = "notif"
urlpatterns = [
    path('',home, name="home"),
    path('gen/',gen,name="gen"),
    path('all_vip/',vip,name="vip"),
    path('latest_vip/',lvip,name="lvip"),
    path('notification/',notif,name="notif"),
    path('customer/',update,name="update"),
    path("update/<int:id>",cupdate,name="cupdate"),
    path("all/",lists,name="all"),
    path("delete/<int:id>/",delete,name="delete"),
    path("add/",add,name="add")
]
