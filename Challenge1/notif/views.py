from django.shortcuts import render, redirect
import json
from django.http import HttpResponse
import datetime
import random
from .models import Reservation, Notification
import smtplib
import ssl
from email import encoders
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart


# Create your views here.
def home(request):
    return render(request, 'notif.html')


def getroom(room):
    data = '{ \
      "room_id_pms": "290007db-0732-48b6-a42e-a20de327916b", \
      "room_name": "515", \
      "room_number": "515", \
      "other_rooms_number": [ ],  \
      "room_type_pms": "2c0f6cf7-1ed5-4bcb-b814-38e9d6914d02",  \
      "room_type": "room", \
      "room_floor": "5", \
      "room_building": "SAVOY", \
      "room_category_name":'+ room + ', \
      "reservation_room_data": { \
        "start_date": "2021-10-04", \
        "end_date": "2021-10-05", \
        "guests": [  "7e860332-d457-4ea1-8515-adb4009c5fe3", "da15c5dc-7411-47ff-a9ed-ad6c00e28b90"], \
        "number_of_keys": 1,  \
        "guest_id_pms": "da15c5dc-7411-47ff-a9ed-ad6c00e28b90",  \
        "guest_first_name": "",  \
        "guest_last_name": "Groupe Cimalpes 04-05 Octobre 2021",  \
        "number_of_children": 0,  \
        "rates": [  ], \
        "balance": 0,  \
        "rate_id_pms": "62cf8b3f-7257-451f-8c21-ed80de8cd166",  \
        "is_available": false,  \
        "reservation_id_pms": "86b7bd80-6ba2-444f-8015-ad6c00e30661",  \
        "number_of_adults": 1,  \
        "room_total": 0  \
      }  \
    }'

    return data

def getguests():
    data = '{' \
           '"guest_id_pms": "7e860332-d457-4ea1-8515-adb4009c5fe3", \
      "title": "", \
      "language": "EN", \
      "first_name": "Grégory", \
      "last_name": "FLON", \
      "email": "", \
      "phone": "", \
      "loyalty_code": "", \
      "address1": "", \
      "address2": "", \
      "gender": "", \
      "city": "", \
      "passport_number": "", \
      "passport_expire_date": null, \
      "passport_issuance_contry_code": "ND", \
      "passport_issuance_date": null, \
      "state": "", \
      "country": "ND", \
      "nationality": "ND", \
      "zip_code": "",  \
      "birthday": null, \
      "document_type": null, \
      "birthday_place": "", \
      "note": "", \
      "is_main_guest": false, \
      "reservation_guest_data": { \
        "start_date": "2021-10-04", \
        "end_date": "2021-10-05" \
      }, \
      "reservation_id_pms": "86b7bd80-6ba2-444f-8015-ad6c00e30661", \
      "room_id_pms": "290007db-0732-48b6-a42e-a20de327916b", \
      "room_number": "515" \
    }'
    return data

def gen(request):
    nb_generation = random.randint(1,20)
    how_many = 0
    for pter in range(nb_generation):
        # Random reservation
        with open('res.json','r') as f:
            data = json.loads(f.read())
        data['customer'] = random.randint(1,3)
        total = random.randint(100,1000)
        nb_guests = random.randint(1,3)
        nb_rooms = random.randint(nb_guests,nb_guests * 2)
        room = random.choice(["king","normale"])
        start = datetime.date.today()
        nights = random.randint(1,15)
        end = start + datetime.timedelta(days=nights)
        guests = []
        for k in range(nb_guests):
            guests.append(getguests())
        rooms = []
        for k in range(nb_rooms):
            rooms.append(getroom(room))
        data["total"] = total
        data["start_date"] = start.strftime("%Y-%m-%d")
        data['end_date'] = end.strftime("%Y-%m-%d")
        data["guests"] = guests
        data['number_of_night'] = nights
        # Create new reservation
        res = Reservation.objects.create(res=json.dumps(data))
        res.save()
        how_many += 1
    return render(request, 'notif.html', {"type": 1, "message": json.dumps(
        {"message": "{} new reservations have been successfully added to the database".format(how_many)})})


def test_power(user,ok):
    users = []
    for k in user:
        data = json.loads(k.res)
        notifications = Notification.objects.filter(user=data['customer'])
        for notification in notifications:
            if notification.conditions != "---":
                conditions = json.loads(notification.conditions)
                send = True
                for condition in conditions:
                    val = condition['value']
                    field = condition['field']
                    operator = condition['operator']
                    print(eval(str(data[field]) + ' ' + operator +' '+ str(val)), str(data[field]) + ' ' + operator +' '+ str(val))
                    send = send and eval(str(data[field]) + ' ' + operator +' ' + str(val))

                send and users.append({"ID": data['customer'], "total": data['total'], "periode": data['number_of_night'],
                         "message": notification.msg,
                         "name": data['first_name_main_guest'] + data['last_name_main_guest']})
    return users


def lvip(request):
    obj = Reservation.objects.filter(vue=False)
    users = test_power(obj,False)
    if(len(users) > 0):
        return render(request, 'notif.html', {"type": 2, "message": users})
    else:
        return render(request, 'notif.html', {"type": 1, "message": "Sorry we did not find any VIPs Client"})

def vip(request):
    obj = Reservation.objects.all()
    users = test_power(obj, False)
    if (len(users) > 0):
        return render(request, 'notif.html', {"type": 2, "message": users})
    else:
        return render(request, 'notif.html', {"type": 1, "message": "Sorry we did not find any VIPs Client"})

def notif(request,obj=Reservation.objects.all(),o=True):
    obj = Reservation.objects.filter(vue=False)
    users = test_power(obj,True)
    if(len(users) > 0):
        sendmail("cezarba1996@gmail.com",users)
        return render(request, 'notif.html',{"type":1, "message": json.dumps({"Code":200, "message": "{} notification sended with specefic message".format(len(users))}) })
    else:
        return render(request, 'notif.html',{"type":1, "message": json.dumps({"Code":200, "message": "We did not get any VIPs clients"}) })


def sendmail(mail, user):
    port = 465  # For SSL
    # Create a secure SSL context
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login("businesstracker88@gmail.com", 'xXx123xXx')

        msg = MIMEMultipart()
        msg['from'] = 'Django Candidat'
        msg['to'] = mail
        msg['Subject'] = "Last step to register"
        ch = '<h1> VIP CLients Notifications </h1> <br> <br> <img src="https://www.lhotellerie-restauration.fr/journal/equipement-materiel/2018-06/img/tablhotel.jpg" alt="check connection" <br>'
        ch += "<ul>"
        for k in range(len(user)):
            #print('------------------------------------ \n {} \n -------------------------------------------'.format(user[k]))
            ch += "<li> id: {} <br> full name: {} <br> number of nights: {} <br> Total: {} <br> Le message de notification est: {} <br></li>".format(user[k]['ID'], user[k]['name'],
                                                                                                       user[k]['periode'], user[k]['total'], user[k]['message'])
        ch += "</ul>"
        message = ch
        msg.attach(MIMEText(message, 'html'))
        server.sendmail('businesstracker88@gmail.com"', mail, msg.as_string())

def update(request):
    if (request.method == "POST"):
        log = request.POST['log']
        pwd = request.POST['pwd']
        obj = Notification.objects.filter(user=log,pwd=pwd,conditions="---",msg="---")
        if(len(obj) > 0):
            request.session[0] = obj[0].user
            request.session[1] = obj[0].pwd
            return redirect('notif:all')
        else:
            return render(request, 'customer.html',{"msg": "Check your identifications"})
    else:
        return render(request,'customer.html')


def lists(request):
    instance = Notification.objects.filter(user=request.session['0'])
    notifs = []
    for obj in instance:
        if(obj.conditions != '---' and obj.msg != '---'):
            notifs.append({"msg":obj.msg,"cond":obj.conditions,"id":obj.id})
    if(len(notifs) > 0):
        return render(request,"add_notifs.html",{"type":1,"notif":notifs})
    else:
        return render(request,"add_notifs.html",{"type":0,"notif":"noting is added"})


def cupdate(request,id):
    if(request.method == "POST"):
        instance = Notification.objects.get(user=request.session['0'],id=id)
        instance.conditions = request.POST['cond']
        instance.msg = request.POST['notif']
        instance.save()
        return render(request, "custom_modif.html",
                      {"cond": instance.conditions, "notif": instance.msg, "msg": "Well updated"})
    else:
        instance = Notification.objects.get(user=request.session["0"],id=id)
        return render(request,"custom_modif.html", {"cond":instance.conditions,"notif":instance.msg, "msg":""})

def delete(request,id):
    ins = Notification.objects.get(id=id,user=request.session['0'])
    ins.delete()
    return redirect("notif:all")

def add(request):
    if(request.method == "POST"):
        cond = request.POST['cond']
        msg = request.POST['notif']
        notif = Notification.objects.create(conditions=cond,msg=msg,user=request.session['0'],pwd=request.session['1'])
        notif.save()
        return HttpResponse("<h1> notification has been successfully added </h1>")
    else:
        return render(request,'add.html')

rooms = {
    "roles":{
        "type":"list",
        "required":True,
        "validation":[{
            "field":"room_id_pms", "roles":{"type":"str", "required":True}
        },
            {
                "field":"room_type_pms", "roles":{"type":"str", "required":True}
            }]
    }
}
validation = [
    {"field":"first_name", "roles": {
        "type": "str",
        "required":True
    }},
    {"field":"last_name", "roles": {
        "type": "str",
        "required":True
    }},
    {"field":"reservation_id_pms", "roles": {
        "type": "str",
        "required":True
    }},
    {
        "field":"rooms", **rooms
    },
    {
        "field":"invoice", "roles": {"type":"dict", "required":False},"convert":True, "validation":[{
        "field":"restervation_additional_products", "roles":{
            "required":False, "type":"list", "validation":[
                {"field":"product_name", "roles":{'type':"str", "required":True}},
                {"field": "product_quantity", "roles": {'type': "int", "required": True}},
                {"field": "product_id", "roles": {'type': "str", "required": True}},
                {"field": "product_unite_price", "roles": {'type': "int", "required": True}},
            ]
        }
    }]
    }
]

