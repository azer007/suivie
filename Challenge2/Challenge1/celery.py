from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

# Set the default Django settings module for the 'celery' program.
os.environ['FORKED_BY_MULTIPROCESS'] = '1'
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Challenge1.settings')

app = Celery('Challenge1') #,backend='amqp', broker='amqp://')

app.config_from_object('django.conf:settings')
# Load task modules from all registered Django apps.
app.autodiscover_tasks()

'''
@app.task()
def debug_task(self):
    print(f'Request: {self.request!r}')

app.conf.beat_schedule = {
    'add-every-30-seconds': {
        'task': 'consumer.tasks.exemple',
        'schedule': crontab(hour=1),
    },
}
'''

'''
@app.task()
def debug_task(self):
    print(f'Request: {self.request!r}')
'''

app.conf.beat_schedule = {
    'add-every-30-seconds': {
        'task': 'consumer.tasks.notification',
        'schedule': crontab(minute=1),
    },
}



