from django.urls import path
from consumer import views


app_name = "consumer"
urlpatterns = [
    path('',views.home,name="home"),
    path('latest_vips/',views.lvip,name="lvip"),
    path("check/",views.stocke,name="datas"),
    path("vips",views.vip,name="vip"),
    path("notification",views.notification,name="notif"),
]
