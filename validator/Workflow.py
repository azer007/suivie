import json
import datetime

'''
###
def global_encoder(data):
    for t,tt in zip(["\"{","\"["],["}\"","]\""]):
        try:
            idx = data.index(t)
            print('hey')
            idxx = data.index(tt)
            j = idx - 1
            end = 0
            block = True
            while(True):
                if(text[j] == "\""):
                    block = not block
                    if(block):
                        keys.append((data[j + 1:end],t))
                        print('heeeeeeeeeeeeere')
                        print(j)
                        print(data[idx:idxx])
                        #data[idx:data.index(coupled[iter])] =  data[idx + 1:data.index(coupled[iter]) - 1]
                        #return data
                    else:
                        end = j
                j -= 1
        except:
            return data

def global_decoder(data):
    for k in keys:
        print(k)

def decoder(data,key,type):
    return json.loads(data[key])

def encoder(data,key,type):
    return json.dumps(data[key])
'''

###################       Rooms conditions to check       ##########################
rooms ={
  "roles": {
    "type": "list",
    "required": True,
    "validation": [
      {
        "field": "room_id_pms",
        "roles": {
          "type": "str",
          "required": True
        }
      },
      {
        "field": "room_type_pms",
        "roles": {
          "type": "str",
          "required": True
        }
      },
      {
        "field": "reservation_room_data",
        "roles": {
          "type": "dict",
          "required": True,
          "validation": [
            {
              "field": "guests",
              "roles": {
                "type": "list",
                "required": True,
                "validation": []
              }
            }
          ]
        }
      }
    ]
  }
}

###################       Invoice conditions to check     ########################
invoice = {
    "roles":{
        "type":"dict",
        "required":False,
        "convert": True,
        "validation":[{
                "field":"reservation_additional_products", "roles":{"required":True, "type":"list",
                "validation":[
                        {"field":"product_name", "roles":{'type':"str", "required":True}},
                        {"field": "product_quantity", "roles": {'type': "int", "required": True}},
                        {"field": "product_id", "roles": {'type': "str", "required": True}},
                        {"field": "product_unite_price", "roles": {'type': "int", "required": True}},
                    ]
                }
            }]
    }
}

###############           Global conditions, it might contain some sub conditions...     ########################
validation = [
    {"field":"first_name_main_guest", "roles": {
        "type": "str",
        "required":True, "validation":[]
    }},
    {"field":"last_name_main_guest", "roles": {
        "type": "str",
        "required":True, "validation":[]
    }},
    {"field":"reservation_id_pms", "roles": {
        "type": "str",
        "required":True, "validation":[]
    }},
    {
        "field":"rooms", **rooms
    },
    {
        "field":"invoice", **invoice
    }
]


err = []
# ******************************              The Validator function              ************************************
def recursions(data, validator, level="data"):
    where = []
    result = True

    for field_valditor in validator:

        if field_valditor['roles']['type'] == "list":
            data_values = json.loads(data[field_valditor['field']]) if 'convert' in field_valditor['roles'] and \
                                          field_valditor['roles']['convert'] else data[field_valditor['field']]

            required = True if not(field_valditor['roles']['required']) else len(data_values) > 0
            _type = type(data_values) == eval(field_valditor['roles']['type'])
            exist = field_valditor['field'] in data

            if(required and _type and exist):
                for iter,data_value in enumerate(data_values):

                    _where, subvalidation = recursions(data_value, field_valditor['roles']['validation'],
                                                       level + '.' + field_valditor['field'])
                    if(not(subvalidation)):

                        err.append({
                            "field": field_valditor['field'],
                            "type": type(data_values).__name__ == field_valditor['roles']['type'],
                            "required": required,
                            "exits": exist,
                            "level": level + field_valditor['field'],
                            "indice": iter
                        })

                        result = False
                    else:
                        result = True
            else:
                err.append({
                    "field": field_valditor['field'],
                    "type": type(data_values).__name__ == field_valditor['roles']['type'],
                    "required": required,
                    "exits": exist,
                    "level": level + field_valditor['field']
                })

                result = False
        else :


            data_value = json.loads(data[field_valditor['field']]) if 'convert' in field_valditor['roles'] and field_valditor['roles']['convert'] else data[field_valditor['field']]

            _type = type(data_value) == eval(field_valditor['roles']['type'])

            exist = field_valditor['field'] in data
            required = not(not(data_value)) if _type != 'bool' else bool(str(data_value))
            subvalidation = True
            _where = []
            #find if has sub validators
            if "validation" in field_valditor['roles']:
                _where , subvalidation = recursions(data_value, field_valditor['roles']['validation'], level+'.'+field_valditor['field'])

            result = required and _type and subvalidation and exist

            not result and where.append({
              "field":field_valditor['field'],
              "type":type(data_value).__name__ == field_valditor['roles']['type'],
              "required":required,
              "exits":exist,
              "subvalidation":_where,
              "level":level
            })

    #print(result)
    return where, result

keys = []
with open("reservation.txt") as f:
    text = f.read()
    data = json.loads(text)

    where, result = recursions(data, validation,'reservation')
    if(len(err) > 0 ):
        print("{} \n \t This Reservation  has an error, check fields below : \n{}".format("-" * 200, "-" * 200))
    else:
        print("This reservation  is well checked")

    for e in err:
        print(json.dumps(e))

